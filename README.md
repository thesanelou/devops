<H1>Git destiné aux travaux lab</H1>
<H2>Usage uniquement personnel, à titre privée</H2>

**Liste des projets**
* Global
* Ansible
* Docker
* Jenkins

**Resumé de l'infra** 
Serveurs virtuels

| Nom de la machine  | OS                 | IP           | Emplacement                       | Descriptif / Utilisation              | 
| :----------------- |:-------------------| ------------:| ---------------------------------:| -------------------------------------:|
| alk-ls-ubu22-main  | Ubuntu 22.04.2 LTS | 192.168.8.22 | ws-thesanelou\c:\virtual machines | Main Jenkins / Docker /Ansible        | 
| alk-ls-ubu22-lab01 | Ubuntu 22.04.2 LTS | dhcp         | ws-thesanelou\c:\virtual machines | Machine éphémère 1                    | 
| alk-ls-ubu22-lab02 | Ubuntu 22.04.2 LTS | dhcp-        | ws-thesanelou\c:\virtual machines | Machine éphémère 1                    |
| alk-ls-deb11-lab01 | Debian 11          | 192.168.8.23 | ws-thesanelou\c:\virtual machines | Machine Docker                        | 

